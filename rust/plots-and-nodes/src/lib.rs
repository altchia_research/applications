use std::cell::RefCell;

use glib::prelude::*;
use glib::subclass::prelude::*;

use glib::translate::*;
use glib::StaticType;

use libc::c_int;
use libc::c_char;
use libc::c_void;

use std::ops;


pub type USNAppPanError = c_int;


#[derive(Debug, Copy, Clone, glib::GEnum)]
#[genum(type_name = "USNAppPanError")]
pub enum Error {
    Parse,
    Forbidden,
    Impossible,
}

mod i_crypto_string {
  use super::*;

  pub mod imp {
    use super::*;

    pub struct ICryptoString(c_void);

    #[derive(Clone, Copy)]
    #[repr(C)]
    pub struct ICryptoStringIface {
      pub parent_iface: glib::gobject_ffi::GTypeInterface,
      pub into_crypto_string: Option<unsafe extern "C" fn(*mut ICryptoString) -> *mut c_char>,
    }

    #[glib::object_interface]
    unsafe impl ObjectInterface for ICryptoStringIface {
      const NAME: &'static str = "USNAppPanICryptoString";
      //type Prerequisites = (glib::Object,);
      //fn interface_init(&mut self) {}
    }
  }

  glib::wrapper! {
    pub struct ICryptoString(Interface<imp::ICryptoString, imp::ICryptoStringIface>);

    match fn {
        type_ => || unimplemented!(),
    }
  }

  unsafe impl<T: ObjectSubclass> IsImplementable<T> for ICryptoString {
    fn interface_init(iface: &mut glib::Interface<Self>) {
        //let iface = iface.as_mut();
        //iface.get_name = Some(get_name_trampoline::<T>);
    }
    fn instance_init(_instance: &mut glib::subclass::InitializingObject<T>) {}
  }

}

use i_crypto_string::ICryptoString;

/*
#[no_mangle]
pub unsafe extern "C" fn usn_app_pan_error_get_type() -> glib::ffi::GType {
	USNAppPanError::static_type().into_glib()
}

#[no_mangle]
pub extern "C" fn usn_app_pan_icrypto_string_get_type() -> glib::ffi::GType {
	<USNAppPanICryptoStringIface as glib::subclass::interface::ObjectInterfaceType>::type_().into_glib()
}
*/

mod public_key {
  use super::*;

  mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct PublicKey {}

    #[repr(C)]
    pub struct PublicKeyClass {
      pub parent_class: glib::gobject_ffi::GObjectClass,
    }

    unsafe impl ClassStruct for PublicKeyClass {
      type Type = PublicKey;
    }

    #[glib::object_subclass]
    impl ObjectSubclass for PublicKey  {
      const NAME: &'static str = "USNAppPanPublicKey";
      type Type = super::PublicKey;
      type ParentType = glib::Object;
      type Class = PublicKeyClass;
      type Interfaces = (ICryptoString,);
      fn class_init(klass: &mut Self::Class) {
        //klass.increment = Some(increment_default_trampoline);
        //klass.incremented = Some(incremented_default_trampoline);
      }
    }

    impl ObjectImpl for PublicKey {}
    
  }

  /*
  glib::wrapper! {
    pub struct PublicKey(Object<imp::PublicKey, imp::PublicKeyClass>) @implements ICryptoString;

    match fn {
        type_ => || unimplemented!(),
    }
  }
  */

  glib::wrapper! {
    pub struct PublicKey(ObjectSubclass<imp::PublicKey>);
  }

}


