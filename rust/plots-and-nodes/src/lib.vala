
// Universal strorage network: USN
// Application: App
// Plots and nodes: Pan

namespace USN.App.Pan {
/******************************************/

/*
public class Result<T, E> : GLib.Object {
	private bool _is_err;
	private GLib.Value value;
	public static Result<T, E> Ok(T a) { return null; }
	public static Result<T, E> Err(E a) { return null; }
	public bool is_ok() { return true; }
	public bool is_err() { return true; }
	public T? ok() { return null; }
	public E? err() { return null; }
}
*/

public enum Error {
	PARSE,
	FORBIDDEN,
	IMPOSSIBLE,
}

public interface ICryptoString : GLib.Object {
	public abstract string into_crypto_string();
}

public class PublicKey : GLib.Object, ICryptoString {
	//public static Result<PublicKey, Error> from_crypto_string(string a) { return null; }
	public string into_crypto_string() { return null; }
}

public class AppDatabase : GLib.Object {
}

public class KeyStore : GLib.Object {
}

public class Plot : GLib.Object {
}

namespace Chia {

	public class PlotId : GLib.Object, ICryptoString {
		public string into_crypto_string() { return null; }
	}

	public class Plot : Pan.Plot {
		public PlotId Id { get; private set; }
	}

}

public class Miner : GLib.Object {

	public PublicKey id { get; private set; }

	public bool is_keystore_available { get; private set; }

	public Miner(PublicKey a) {}

	public signal void on_keystore_available();

	public signal void on_block_solved();

}

public class Node : GLib.Object {
}

/******************************************/ }
